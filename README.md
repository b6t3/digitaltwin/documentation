# Documentation

This is the documentation project for the **B6T3 - DigitalTwin simulation suite**. 

The simulation suite aims to provide an easy to use playground for the evaluation of new design concepts for the networks of the future and makes use of the excellent MiniNet toolset.

The documentation is intended for following audience:

- **managers**: who want to get a basic understanding of future concepts
- **administrators**: who will need to manage simulation environments for different experts
- **network engineers**: who are knowledgeable of python and want to experiment with new concepts
- **developers**: who will want to contribute to this simulation suite. 

---

**License**: Apache 2.0